class Post < ActiveRecord::Base
	belongs_to :user 
	has_many :comments, dependent: :destroy
	validates :title, presence: true
#   validates :title, presence: {message: "Title is invalid ,So post is not saved"}
    validates :body, presence: true
    #validates :body, presence: {message: "Body is invalid,So post is not saved"}
end
